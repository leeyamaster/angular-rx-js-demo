import {Component} from '@angular/core';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';
import {NzResizeEvent} from "ng-zorro-antd/resizable";
import * as moment from "moment";
import {fromEvent, map, switchMap, takeUntil} from "rxjs";

interface Person {
  key: string;
  name: string;
  age: number;
  address: string;
}

@Component({
  selector: 'app-my-table',
  templateUrl: './my-table.component.html',
  styleUrls: ['./my-table.component.less']
})
export class MyTableComponent {
  column: {
    title: string,
    width: number
  }[] = [
    {
      title: 'name',
      width: 200
    },
    {
      title: 'title',
      width: 200
    },
    {
      title: 'address',
      width: 200
    },
  ]

  constructor() {

  }

  move(column: {
    title: string,
    width: number
  }, e: any) {
    console.log(e)
    const {title, width} = column
    console.log(width)
    const dataTarget = this.column.find(v => v.title === title)

    const box = e.currentTarget
    fromEvent(box, "mousedown")
      .pipe(
        map((event:any) => ({
          distanceX: event.clientX - event.target.offsetLeft,
          distanceY: event.clientY - event.target.offsetTop
        })),
        switchMap(({ distanceX, distanceY }) =>
          fromEvent(document, "mousemove").pipe(
            map((event:any) => ({
              positionX: event.clientX - distanceX,
              positionY: event.clientY - distanceY
            })),
            takeUntil(fromEvent(document, "mouseup"))
          )
        )
      )
      .subscribe(({ positionX, positionY }) => {
        console.log('positionX',positionX)
        // @ts-ignore
        dataTarget.width = width + positionX
      })
  }
}
