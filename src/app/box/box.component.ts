import {Component, ViewChild} from '@angular/core';
import {NzResizeDirection, NzResizeEvent} from "ng-zorro-antd/resizable";
import {fromEvent, map, switchMap, takeUntil} from "rxjs";

@Component({
  selector: 'app-box',
  templateUrl: './box.component.html',
  styleUrls: ['./box.component.less']
})
export class BoxComponent {
  @ViewChild('leftDiv') leftDiv: any
  leftDivWidth = 300;
  move(e: any) {
    console.log(e)

    const box = e.currentTarget
    fromEvent(box, "mousedown")
      .pipe(
        map((event:any) => ({
          distanceX: event.clientX - event.target.offsetLeft,
          distanceY: event.clientY - event.target.offsetTop
        })),
        switchMap(({ distanceX, distanceY }) =>
          fromEvent(document, "mousemove").pipe(
            map((event:any) => ({
              positionX: event.clientX - distanceX,
              positionY: event.clientY - distanceY
            })),
            takeUntil(fromEvent(document, "mouseup"))
          )
        )
      )
      .subscribe(({ positionX, positionY }) => {
        console.log('positionX',positionX)
        this.leftDivWidth = this.leftDivWidth + positionX
      })
  }
}
