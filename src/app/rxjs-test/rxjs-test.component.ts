import {Component} from '@angular/core';
import {
  catchError,
  combineLatest,
  concat, concatAll, concatMap, debounceTime, delayWhen, distinctUntilChanged, first,
  from,
  fromEvent, groupBy,
  interval, last, map, mapTo,
  merge, mergeAll, mergeMap,
  Observable,
  of, pluck, race,
  range, reduce,
  ReplaySubject, scan, skip, skipUntil, skipWhile, startWith, switchMap,
  take, takeUntil, takeWhile, throttleTime, throwError,
  timer, toArray, withLatestFrom, zip
} from "rxjs";

@Component({
  selector: 'app-rxjs-test',
  templateUrl: './rxjs-test.component.html',
  styleUrls: ['./rxjs-test.component.less']
})
export class RxjsTestComponent {
  ngOnInit() {
    this.test()
  }

  test() {
    const box = document.getElementById("box") as any;
    // box.onmousedown = function (event: { clientX: number; target: { offsetLeft: number; offsetTop: number; }; clientY: number; }) {
    //   let distanceX = event.clientX - event.target.offsetLeft
    //   let distanceY = event.clientY - event.target.offsetTop
    //   document.onmousemove = function (event) {
    //     let positionX = event.clientX - distanceX
    //     let positionY = event.clientY - distanceY
    //     box.style.left = positionX + "px"
    //     box.style.top = positionY + "px"
    //   }
    //   box.onmouseup = function () {
    //     document.onmousemove = null
    //   }
    // }
    // drag
    fromEvent(box, "mousedown")
      .pipe(
        map((event: any) => ({
          distanceX: event.clientX - event.target.offsetLeft,
          distanceY: event.clientY - event.target.offsetTop
        })),
        switchMap(({ distanceX, distanceY }) =>
          fromEvent(document, "mousemove").pipe(
            map((event: any) => ({
              positionX: event.clientX - distanceX,
              positionY: event.clientY - distanceY
            })),
            takeUntil(fromEvent(document, "mouseup"))
          )
        )
      )
      .subscribe(({ positionX, positionY }) => {
        box.style.left = positionX + "px"
        box.style.top = positionY + "px"
      })

    // search
    const search = document.getElementById("search") as HTMLDivElement;
    fromEvent(search, "keyup")
      .pipe(
        debounceTime(700),
        map((event: any) => event.target.value),
        distinctUntilChanged(),
        switchMap(keyword =>
          from(
            [{
              data: 123
            }]
          ).pipe(
            map(response => response.data),
            catchError(error => throwError(`发生了错误: ${error.message}`))
          )
        )
      )
      .subscribe({
        next: value => {
          console.log(value)
        },
        error: error => {
          console.log(error)
        }
      })

    // 串联
    const button = document.getElementById("button") as HTMLDivElement;
    fromEvent(button, "click")
      .pipe(
        concatMap(event =>
          from([{data:123,token:456,message: "aaa"}]).pipe(
            map(r => ({data:r.data,token:r.token}))
          )
        ),
        concatMap(data =>{
          console.log('data',data)
          return of({data: 'hello'})
        })
      )
      .subscribe(console.log)
    // fromEvent(document.body, 'click') // 註冊監聽
    //   .pipe(take(1)) // 只取一次
    //   .subscribe(console.log);
    // range(0, 5).subscribe(n => console.log(n))

    // timer(2000).subscribe(n => console.log(n))


    // fromEvent(document, "click")
    //   .pipe(switchMap(ev => interval(1000)))
    //   .subscribe(x => console.log(x))
  }
}
